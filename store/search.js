export const state = () => ({
  results: []
})

export const mutations = {
  set(state, results) {
    state.results = results
  },
  remove(state) {
    state.results = []
  }
}
